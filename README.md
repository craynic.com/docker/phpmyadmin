# phpMyAdmin

Enhanced phpMyAdmin image with HTTPS support and various preset
variables.

## Environment variables

On top of the ENV variables supported by the
[base image](https://hub.docker.com/r/phpmyadmin/phpmyadmin/) and also
those cleverly hidden in
[phpMyAdmin's PHP configuration files](https://github.com/phpmyadmin/docker/blob/master/apache/config.inc.php),
this container supports some additional ENV variables:

* `PMA_DENIED_USERS`: comma-separated list of usersnames to deny login to
* `PMA_MYSQL_CERTIFICATE`: MySQL server's CA certificate; this will enable SSL
    connection to the MySQL server

## Local Development

### With `docker-compose`

Run
```shell script
docker-compose up --build
```

### Without `docker-compose`

First, create the database:

```shell script
docker run -dit -p 3306:3306 \
  -e MYSQL_ALLOW_EMPTY_PASSWORD="yes" \
  -e MYSQL_DATABASE="db" \
  -e MYSQL_USER="user" \
  -e MYSQL_PASSWORD="pass" \
  --rm --name "phpmyadmin-mysql" \
  "mysql:8"
```

Then build the phpMyAdmin custom container:

```shell script
docker build . -t phpmyadmin-craynic
```

And finally run the container:

```shell script
docker run -dit -p 80:80 -p 443:443 \
  --rm --name "phpmyadmin-craynic" \
  -e PMA_HOST="172.17.0.1" \
  -e PMA_PORT="3306" \
  -e PMA_PMADB="db" \
  -e PMA_CONTROLUSER="root" \
  -e PMA_CONTROLPASS="" \
  "phpmyadmin-craynic"
```
