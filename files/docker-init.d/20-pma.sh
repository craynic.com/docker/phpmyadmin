#!/usr/bin/env bash

set -Eeuo pipefail

PMA_CONFIG_DIR="/etc/phpmyadmin"
PMA_USER_CONFIG_FILE="$PMA_CONFIG_DIR/config.user.inc.php"
PMA_MYSQL_CACERT_FILENAME="$PMA_CONFIG_DIR/ca_cert.pem"

if [[ -n "$PMA_MYSQL_CERTIFICATE" ]]; then
  echo "Installing MySQL certificate to $PMA_MYSQL_CACERT_FILENAME"
  echo "$PMA_MYSQL_CERTIFICATE" > $PMA_MYSQL_CACERT_FILENAME

  cat <<EOF >> "$PMA_USER_CONFIG_FILE"
\$cfg['Servers'][\$i]['ssl'] = true;
\$cfg['Servers'][\$i]['ssl_ca'] = '$PMA_MYSQL_CACERT_FILENAME';
EOF
fi

if [[ -n "$PMA_DENIED_USERS" ]]; then
  DENIED_CONF="$(echo "${PMA_DENIED_USERS//[[:space:]]/}" \
    | grep -v "^$" \
    | tr "," "\n" \
    | sed "s/^\(.*\)$/'deny \1 from all'/" \
    | tr "\n" ",")"

  cat <<EOF >> "$PMA_USER_CONFIG_FILE"
\$cfg['Servers'][\$i]['AllowDeny']['order'] = 'deny,allow';
\$cfg['Servers'][\$i]['AllowDeny']['rules'] = [$DENIED_CONF];
EOF
fi
