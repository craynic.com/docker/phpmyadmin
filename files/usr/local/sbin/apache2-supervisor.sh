#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d/"

/usr/bin/supervisord -c /etc/supervisord.conf -n