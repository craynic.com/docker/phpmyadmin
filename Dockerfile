FROM phpmyadmin/phpmyadmin:5.2.2-fpm-alpine@sha256:1d404a2938acc06e4f26f107f1a460afdd9150ff26f0375ed4eac70a6ca482bf

# renovate: datasource=repology depName=alpine_3_21/apache2 depType=dependencies versioning=loose
ARG APACHE2_VERSION="2.4.62-r0"

RUN apk --no-cache add \
        run-parts~=4 \
        supervisor~=4 \
        apache2="${APACHE2_VERSION}" \
        apache2-proxy="${APACHE2_VERSION}" \
        apache2-ssl="${APACHE2_VERSION}" \
        iproute2~=6 \
    && rm /var/www/logs /var/www/run \
    && rm -rf /var/www/localhost \
    # security issues not fixed in the base package
    && apk --no-cache upgrade \
        # affected versions: 1.36.1-r0
        busybox busybox-binsh ssl_client \
        # affected versions: 1.3.1-r0
        libwebp \
        # affected versions: 3.1.1-r1
        libcrypto3 libssl3 openssl

COPY files/ /

ENV PMA_QUERYHISTORYDB="true" \
    PMA_QUERYHISTORYDB=100 \
    HIDE_PHP_VERSION="true" \
    UPLOAD_LIMIT="64M" \
    PMA_DENIED_USERS="" \
    PMA_MYSQL_CERTIFICATE=""

EXPOSE 10080 10443 20080 20443

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh" \
    9000 10080 10443 20080 20443

CMD ["apache2-supervisor.sh"]
